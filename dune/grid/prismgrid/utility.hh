#ifndef DUNE_GRID_PRISMGRID_UTILITY_HH
#define DUNE_GRID_PRISMGRID_UTILITY_HH

namespace Dune
{

  // Helper methods
  // --------------

  template< int a, int b >
  struct static_min
  {
    static const int v = ( a < b ) ? a : b;
  };



  template< class DomainType, class RangeType >
  RangeType map ( const DomainType &arg )
  {
    RangeType ret( 0 );
    const int entries = static_min< DomainType::dimension, RangeType::dimension >::v;
    for( int i = 0; i < entries; ++i )
      ret[ i ] = arg[ i ];
    return ret;
  }

} // namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_GEOMETRY_HH
