#ifndef DUNE_GRID_PRISMGRID_ENTITYITERATOR_HH
#define DUNE_GRID_PRISMGRID_ENTITYITERATOR_HH

#include <dune/common/typetraits.hh>

#include <dune/grid/common/entityiterator.hh>

#include "entitypointer.hh"

namespace Dune
{

  // PrismEntityIteratorTraits
  // -------------------------

  template< int codim, PartitionIteratorType pitype, class Grid >
  class PrismEntityIteratorTraits
  : public PrismEntityPointerTraits< codim, Grid >
  {
    typedef PrismEntityPointerTraits< codim, Grid > Base;

  public:
    typedef typename Base::Traits Traits;
    typedef typename Base::HostGridView HostGridView;

    typedef typename HostGridView::template Codim<
        Traits::Map::template Codim< codim >::v 
      >::template Partition< pitype >::Iterator HostIterator;
  };



  // PrismEntityIterator
  // -------------------

  template< int codim, PartitionIteratorType pitype, class GridImp >
  class PrismEntityIterator
  : public PrismEntityPointer< PrismEntityIteratorTraits< codim, pitype, GridImp > >
  {
    typedef PrismEntityIterator< codim, pitype, GridImp > This;

    typedef PrismEntityIteratorTraits< codim, pitype, GridImp > Traits;
    typedef PrismEntityPointer< Traits > Base;

  public:
    typedef typename Base::Grid Grid;
    typedef typename Base::HostIterator HostIterator;

    typedef typename Base::Entity Entity;
    typedef typename Base::EntityImp EntityImp;

  protected:
    using Base::hostIterator_;
    using Base::entity_;
    using Base::layers;

  public:
    using Base::level;

    PrismEntityIterator ( const HostIterator &hostIterator,
                          const Grid &grid )
    : Base( hostIterator, grid )
    { 
      const bool isLateral = ( Base::codimension == 0 );
      end_ = ( isLateral ? layers() : layers() + 1 );
    }

    static PrismEntityIterator begin ( const Grid &grid )
    {
      const HostIterator &hostIterator
        = grid.hostGridView().template begin< HostIterator::codimension, pitype >();
      return This( hostIterator, grid );
    }

    static PrismEntityIterator end ( const Grid &grid )
    {
      const HostIterator &hostIterator
        = grid.hostGridView().template end< HostIterator::codimension, pitype >();
      return This( hostIterator, grid );
    }

    void increment ()
    {
      const int subIndex = Base::subIndex() + 1;

      if( subIndex >= end_ )
      {
        entity_.impl().invalidate();
        ++hostIterator_;
      }
      else
        entity_.impl().reset( subIndex );
    }

  private:
    int end_;
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_ENTITYITERATOR_HH
