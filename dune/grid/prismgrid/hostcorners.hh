#ifndef DUNE_GRID_PRISMGRID_HOSTCORNERS_HH
#define DUNE_GRID_PRISMGRID_HOSTCORNERS_HH

#include <dune/grid/common/entity.hh>

#include "entity.hh"

namespace Dune
{

  namespace GeoGrid
  {

    // HostCorners
    // -----------

    template< class HostEntity >
    class HostCorners;

#if DUNE_GRID_EXPERIMENTAL_GRID_EXTENSIONS
    template< int dim, class Grid >
    class HostCorners< Dune::Entity< 0, dim, Grid, Dune::PrismEntity > >
    {
      typedef Dune::Entity< 0, dim, Grid, Dune::PrismEntity > HostEntity;
      typedef Dune::PrismEntity< 0, dim, Grid > HostEntityImpl;

      static const int mydimension = HostEntity::mydimension;

      typedef HostCorners< typename HostEntityImpl::HostEntity > HostHostCorners;
      typedef typename HostEntityImpl::NormalGeometry NormalGeometry;

    public:
      typedef typename HostEntity::Geometry::GlobalCoordinate Coordinate;

      explicit HostCorners ( const HostEntity &hostEntity )
      : hostHostCorners_( hostEntity.impl().hostEntity() ),
        normalGeometry_( hostEntity.impl().normalGeometry() )
      {}

      GeometryType type () const
      {
        return GeometryType( hostHostCorners_.type().id() | (1u << (mydimension-1)), mydimension );
      }

      Coordinate corner ( int i ) const
      {
        const unsigned int numHostCorners = hostHostCorners_.numCorners();
        const typename HostHostCorners::Coordinate hostCorner = hostHostCorners_.corner( i % numHostCorners );

        Coordinate corner;
        for( int j = 0; j < Coordinate::dimension-1; ++j )
          corner[ j ] = hostCorner[ j ];
        corner[ Coordinate::dimension-1 ] = normalGeometry_.corner( i / numHostCorners );

        return corner;
      }

      unsigned int numCorners () const { return 2*hostHostCorners_.numCorners(); }

    private:
      HostHostCorners hostHostCorners_;
      NormalGeometry normalGeometry_;
    };

    template< int dim, class Grid >
    class HostCorners< Dune::Entity< dim, dim, Grid, Dune::PrismEntity > >
    {
      typedef Dune::Entity< dim, dim, Grid, Dune::PrismEntity > HostEntity;
      typedef Dune::PrismEntity< dim, dim, Grid > HostEntityImpl;

      static const int mydimension = HostEntity::mydimension;

      typedef HostCorners< typename HostEntityImpl::HostEntity > HostHostCorners;
      typedef typename HostEntityImpl::NormalGeometry NormalGeometry;

    public:
      typedef typename HostEntity::Geometry::GlobalCoordinate Coordinate;

      explicit HostCorners ( const HostEntity &hostEntity )
      : hostHostCorners_( hostEntity.impl().hostEntity() ),
        normalGeometry_( hostEntity.normalGeometry() )
      {}

      GeometryType type () const
      {
        return GeometryType( hostHostCorners_.type().id(), mydimension );
      }

      Coordinate corner ( int i ) const
      {
        const typename HostHostCorners::Coordinate hostCorner = hostHostCorners_.corner( i );

        Coordinate corner;
        for( int i = 0; i < Coordinate::dimension-1; ++i )
          corner[ i ] = hostCorner[ i ];
        corner[ Coordinate::dimension-1 ] = normalGeometry_.corner( 0 );

        return corner;
      }

      unsigned int numCorners () const { return hostHostCorners_.numCorners(); }

    private:
      HostHostCorners hostHostCorners_;
      NormalGeometry normalGeometry_;
    };
#endif // #if DUNE_GRID_EXPERIMENTAL_GRID_EXTENSIONS

  } // namespace GeoGrid

} // namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_HOSTCORNERS_HH
