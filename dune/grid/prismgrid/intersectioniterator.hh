#ifndef DUNE_GRID_PRISMGRID_INTERSECTIONITERATOR_HH
#define DUNE_GRID_PRISMGRID_INTERSECTIONITERATOR_HH

#include <dune/common/typetraits.hh>

#include <dune/grid/common/intersectioniterator.hh>
#include <dune/grid/common/intersection.hh>

#include "gridenums.hh"
#include "intersection.hh"

namespace Dune
{

  // Forward declaration
  // -------------------

  template< class > class PrismIntersection;



  // PrismIntersectionIterator
  // -------------------------

  template< class GridImp >
  class PrismIntersectionIterator
  {
    typedef PrismIntersectionIterator< GridImp > This;

    typedef typename remove_const< GridImp >::type::Traits Traits;

    typedef typename Traits::Grid Grid;

    typedef typename Traits::template Codim< 0 >::Entity Entity;
    typedef typename Entity::Implementation EntityImp;

    typedef typename Traits::HostGridView HostGridView;
    typedef typename HostGridView::IntersectionIterator HostIntersectionIterator;
    typedef typename HostGridView::Intersection HostIntersection;

    typedef PrismIntersection< const Grid > IntersectionImp;

  public:
    typedef Dune::Intersection< const Grid, IntersectionImp > Intersection;

    PrismIntersectionIterator ( const HostIntersectionIterator &hostIterator,
                                const PrismIntersectionPosition &face,
                                const EntityImp &inside,
                                const Grid &grid )
    : intersection_( IntersectionImp( face, inside, grid ) ),
      hostIterator_( hostIterator )
    { 
      if( face == lateralFace )
        return;

      const bool ghost = ( inside.partitionType() == GhostEntity );
      if( ghost )
      {
        // we begin with lateral intersection, thus increment twice!
        increment();
        increment();
      }
    }

    PrismIntersectionIterator ( const This &other )
    : intersection_( other.intersection_.impl() ),
      hostIterator_( other.hostIterator_ )
    {}

    const This &operator= ( const This &other )
    {
      intersection_.impl() = other.intersection_.impl();
      hostIterator_ = other.hostIterator_;
      return *this;
    }

    static PrismIntersectionIterator begin ( const Entity &inside, const Grid &grid )
    {
      return begin( inside.impl(), grid );
    }

    static PrismIntersectionIterator begin ( const EntityImp &inside, const Grid &grid )
    {
      const HostIntersectionIterator hostIterator
        = grid.hostGridView().ibegin( inside.hostEntity() );
      return This( hostIterator, lowerFace, inside, grid );
    }

    static PrismIntersectionIterator end ( const Entity &inside, const Grid &grid )
    {
      return end( inside.impl(), grid );
    }

    static PrismIntersectionIterator end ( const EntityImp &inside, const Grid &grid )
    {
      const HostIntersectionIterator hostIterator
        = grid.hostGridView().iend( inside.hostEntity() );
      return This( hostIterator, lateralFace, inside, grid );
    }

    bool equals ( const This &other ) const
    {
      return ( ( intersection_.impl().face() == other.intersection_.impl().face() )
                 && intersection_.impl().subIndex() == other.intersection_.impl().subIndex()
                 && ( hostIterator_ == other.hostIterator_ ) );
    }

    void increment ()
    {
      intersection_.impl().invalidate();

      if( intersection_.impl().face() == lateralFace )
        ++hostIterator_;
      ++(intersection_.impl().face());
    }

    const Intersection &dereference () const
    {
      if( intersection_.impl().face() == lateralFace )
      {
        if( !(intersection_.impl().hasHostIntersection()) )
          intersection_.impl().provideHostIntersection( *hostIterator_ );
      }
      return intersection_;
    }

  private:
    const EntityImp &inside () const { return intersection_.impl().insideEntity(); }

    mutable Intersection intersection_;
    HostIntersectionIterator hostIterator_;
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_INTERSECTIONITERATOR_HH
