set( HEADERS
  boundarysegmentindexset.hh
  capabilities.hh
  columniterator.hh
  datahandle.hh
  declaration.hh
  dgfparser.hh
  entity.hh
  entityiterator.hh
  entitypointer.hh
  entityseed.hh
  geometryhelper.hh
  geometry.hh
  gridenums.hh
  gridfamily.hh
  grid.hh
  hierarchiciterator.hh
  hostcorners.hh
  hostgridaccess.hh
  idset.hh
  indexset.hh
  intersection.hh
  intersectioniterator.hh
  normalgrid.hh
  persistentcontainer.hh
  tangentialgeometry.hh
  utility.hh
)

install( FILES ${HEADERS} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/grid/prismgrid )
