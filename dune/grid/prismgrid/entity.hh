#ifndef DUNE_GRID_PRISMGRID_ENTITY_HH
#define DUNE_GRID_PRISMGRID_ENTITY_HH

#include <cassert>

#include <dune/common/exceptions.hh>
#include <dune/common/nullptr.hh>
#include <dune/common/static_assert.hh>
#include <dune/common/typetraits.hh>

#include <dune/geometry/referenceelements.hh>

#include <dune/grid/common/entity.hh>

namespace Dune
{

  // Forward declaration
  // -------------------

  template< int, int, class > class PrismEntity;
  template< int, int, class > class PrismLocalGeometry;
  template< int, int, class > class PrismSubEntity;



  // PrismEntityBaseTraits
  // ---------------------

  template< int codim, int dim, class GridImp >
  struct PrismEntityBaseTraits
  {
    typedef typename remove_const< GridImp >::type::Traits Traits;

    typedef typename Traits::Grid Grid;
    typedef typename Traits::HostGridView HostGridView;

    typedef typename Traits::ctype ctype;

    static const int dimension = Traits::dimension;
    dune_static_assert( dimension == dim, "Template parameters do not match" );
    static const int codimension = codim; 
    static const int dimensionworld = Traits::dimensionworld;
    static const int mydimension = dimension - codimension; 

    typedef typename Traits::template Codim< codimension >::Geometry Geometry;
    typedef typename Geometry::Implementation GeometryImp;
    typedef PrismEntity< codimension, dimension, const Grid > EntityImp;
    typedef typename Traits::template Codim< codimension >::EntitySeed EntitySeed;

    typedef typename HostGridView::template Codim< 
        Traits::Map::template Codim< codim >::v
      >::Entity HostEntity;
  };



  // PrismEntityBase
  // ---------------

  template< class Traits >
  class PrismEntityBase
  {
    typedef PrismEntityBase< Traits > This;

  public:
    typedef typename Traits::Grid Grid;

    typedef typename Traits::ctype ctype;

    static const int codimension = Traits::codimension; 
    static const int dimension = Traits::dimension;
    static const int dimensionworld = Traits::dimensionworld;
    static const int mydimension = Traits::mydimension; 

    typedef typename Traits::HostEntity HostEntity;

    typedef typename Traits::Geometry Geometry;
    typedef typename Traits::GeometryImp GeometryImp;
    typedef typename GeometryImp::NormalGeometry NormalGeometry;

    typedef typename Traits::EntitySeed EntitySeed;
   
  public:
    explicit PrismEntityBase ( const Grid &grid )
    : hostEntity_( nullptr ),
      subIndex_( 0 ),
      grid_( &grid )
    {}

    PrismEntityBase ( const HostEntity &hostEntity, int subIndex, const Grid &grid )
    : hostEntity_( &hostEntity ),
      subIndex_( subIndex ),
      grid_( &grid )
    {}

    int level () const { return 0; }

    PartitionType partitionType () const { return hostEntity().partitionType(); }
    
    Geometry geometry () const
    {
      return Geometry( GeometryImp( hostEntity().geometry(), normalGeometry() ) );
    }

    GeometryType type () const { return geometry().type(); }

    EntitySeed seed () const { return EntitySeed( hostEntity().seed(), subIndex() ); }

    const HostEntity &hostEntity () const
    {
      assert( hostEntity_ );
      return *hostEntity_; 
    }

    int subIndex () const { return subIndex_; }

    const Grid &grid () const 
    {
      assert( grid_ );
      return *grid_;
    }

    operator bool () const { return hostEntity_; }

    void reset ( int subIndex ) { subIndex_ = subIndex; }

    void reset ( const HostEntity &hostEntity ) { hostEntity_ = &hostEntity; }

    void reset ( const HostEntity &hostEntity, int subIndex )
    {
      reset( hostEntity );
      reset( subIndex );
    }

    void invalidate ()
    {
      hostEntity_ = nullptr;
      subIndex_ = 0;
    }

    NormalGeometry normalGeometry () const
    {
      return grid().normalGrid().template geometry< 1 - NormalGeometry::mydimension >( subIndex() );
    }

  private:
    const HostEntity *hostEntity_;
    int subIndex_;
    const Grid *grid_;
  };



  // Entity, codim == 0
  // ------------------

  template< int dim, class GridImp >
  class PrismEntity< 0, dim, GridImp >
  : public PrismEntityBase< PrismEntityBaseTraits< 0 , dim, GridImp > >
  {
    typedef PrismEntity< 0, dim, GridImp > This;
    typedef PrismEntityBase< PrismEntityBaseTraits< 0 , dim, GridImp > > Base;

    typedef typename remove_const< GridImp >::type::Traits Traits;

  public:
    typedef typename Base::Grid Grid;

    typedef typename Base::ctype ctype;

    using Base::codimension;
    using Base::dimension;
    using Base::dimensionworld;
    using Base::mydimension;

    typedef typename Base::Geometry Geometry;
    typedef typename Base::GeometryImp GeometryImp;

    typedef typename Traits::template Codim< 0 >::LocalGeometry LocalGeometry;
    typedef PrismLocalGeometry< mydimension, dimension, const Grid > LocalGeometryImp;

    template< int cd >
    struct Codim
    {
      typedef typename Traits::template Codim< cd >::EntityPointer EntityPointer;
    };
    typedef typename Codim< 0 >::EntityPointer EntityPointer;

    typedef typename Traits::HierarchicIterator HierarchicIterator;

    typedef typename Traits::LeafIntersectionIterator LeafIntersectionIterator;
    typedef typename Traits::LevelIntersectionIterator LevelIntersectionIterator;

    typedef typename Base::EntitySeed EntitySeed;

    typedef typename Base::HostEntity HostEntity;

 public:
    using Base::grid;
    using Base::hostEntity;
    using Base::level;
    using Base::subIndex;
    using Base::type;

    explicit PrismEntity ( const Grid &grid ) : Base( grid ) {}

    PrismEntity ( const HostEntity &hostEntity, int subIndex, const Grid &grid )
    : Base( hostEntity, subIndex, grid )
    {}

    template< int cc >
    int count () const
    {
      return ReferenceElements< ctype, dimension >::general( type() ).size( cc );
    }

    template< int cd >
    typename Codim< cd >::EntityPointer subEntity ( int i ) const
    {
      return PrismSubEntity< cd, dim, Grid >::entityPointer( i, *this );
    }

    LeafIntersectionIterator ileafbegin () const
    {
      typedef typename LeafIntersectionIterator::Implementation IteratorImp;
      return IteratorImp::begin( *this, grid() );
    }

    LeafIntersectionIterator ileafend () const
    {
      typedef typename LeafIntersectionIterator::Implementation IteratorImp;
      return IteratorImp::end( *this, grid() );
    }

    LevelIntersectionIterator ilevelbegin () const { return ileafbegin(); }

    LevelIntersectionIterator ilevelend() const { return ileafend(); }

    EntityPointer father () const
    {
      DUNE_THROW( InvalidStateException, "Called father() on macro grid entity" );
    }

    bool hasFather () const { return false; }

    bool isLeaf () const { return true; }

    bool isRegular () const { return true; }

    const LocalGeometry geometryInFather () const
    {
      DUNE_THROW( InvalidStateException, "Called geometryInFather() on macro grid entity" );
    }

    HierarchicIterator hbegin ( int maxLevel ) const
    {
      typedef typename HierarchicIterator::Implementation IteratorImp;
      return IteratorImp::begin( maxLevel, *this, grid() );
    }

    HierarchicIterator hend ( int maxLevel ) const
    {
      typedef typename HierarchicIterator::Implementation IteratorImp;
      return IteratorImp::end( maxLevel, *this, grid() );
    }

    bool isNew () const { return false; }

    bool mightVanish () const { return false; }

    bool hasBoundaryIntersections () const
    {
      return ( hostEntity().hasBoundaryIntersections() || subIndex() == 0 || subIndex() == grid().layers()-1 );
    }
  };



  // PrismEntity, codim == dim
  // -------------------------

  template< int dim, class GridImp >
  class PrismEntity< dim, dim, GridImp >
  : public PrismEntityBase< PrismEntityBaseTraits< dim, dim, GridImp > >
  {
    typedef PrismEntity< dim, dim, GridImp > This;
    typedef PrismEntityBase< PrismEntityBaseTraits< dim, dim, GridImp > > Base;

  public:
    typedef typename Base::Grid Grid;

    explicit PrismEntity ( const Grid &grid ) : Base( grid ) {}

    PrismEntity ( const typename Base::HostEntity &hostEntity, int subIndex, const Grid &grid )
    : Base( hostEntity, subIndex, grid )
    {}
  };



  // PrismSubEntity
  // --------------

  template< int dim, class Grid >
  struct PrismSubEntity< 0, dim, Grid >
  {
    typedef typename remove_const< Grid >::type::Traits Traits;

    typedef typename Traits::template Codim< 0 >::Entity Entity;
    typedef typename Entity::Implementation EntityImp;

    static const int codimension = 0;
    typedef typename Traits::template Codim< codimension >::EntityPointer EntityPointer;

    static EntityPointer entityPointer ( int i, const EntityImp &entity )
    {
      assert( i == 0 );
      typedef typename EntityPointer::Implementation EntityPointerImp;
      return EntityPointer( EntityPointerImp( entity ) );
    }
  };

  template< int dim, class GridImp >
  struct PrismSubEntity< dim, dim, GridImp >
  {
    typedef typename remove_const< GridImp >::type::Traits Traits;
    typedef typename Traits::Grid Grid;

    typedef typename Traits::template Codim< 0 >::Entity Entity;
    typedef typename Entity::Implementation EntityImp;

    static const int codimension = dim;
    typedef typename Traits::template Codim< codimension >::EntityPointer EntityPointer;

    static EntityPointer entityPointer ( int i, const EntityImp &entity )
    {
      // get host entity
      typedef typename Traits::HostGridView HostGridView;
      const typename HostGridView::template Codim< 0 >::Entity &
        hostEntity = entity.hostEntity();

      // get host vertex
      int count = hostEntity.template count< HostGridView::dimension >();
      typename HostGridView::template Codim< HostGridView::dimension >::EntityPointer
        hostEntityPointer = hostEntity.template subEntity< HostGridView::dimension >( i%count );

      // create entity pointer
      int subIndex = entity.subIndex();
      const Grid &grid = entity.grid();
      typedef typename EntityPointer::Implementation EntityPointerImp;
      return EntityPointer( EntityPointerImp( hostEntityPointer, subIndex + i/count, grid ) );
    }
  };

} // end namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_ENTITY_HH
