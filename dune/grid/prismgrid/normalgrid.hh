#ifndef DUNE_FEM_GRID_PRISMGRID_NORMALGRID_HH
#define DUNE_FEM_GRID_PRISMGRID_NORMALGRID_HH

#include <algorithm>
#include <cassert>

#include <dune/common/static_assert.hh>

namespace Dune
{

  //  Forward declarations
  //  --------------------

  template< class > class PrismInterval;
  template< class, int > class PrismNormalGeometry;



  // PrismNormalGrid
  // ---------------

  template< class ct >
  class PrismNormalGrid
  {
    typedef PrismNormalGrid< ct > This;

  public:
    typedef ct ctype;

    static const int dimension = 1;
    static const int dimensionworld = 1;

    typedef PrismInterval< ctype > Interval;

    template< int codim >
    struct Codim
    {
      typedef PrismNormalGeometry< ctype, dimension - codim > Geometry; 
      typedef Geometry LocalGeometry;
    };

    PrismNormalGrid ( ctype bound[ 2 ], int cells )
    : interval_( bound ),
      macroCells_( cells )
    {}

    PrismNormalGrid ( ctype a, ctype b, int cells )
    : interval_( a, b ),
      macroCells_( cells )
    {}

    PrismNormalGrid ( const Interval &interval, int cells )
    : interval_( interval ),
      macroCells_( cells )
    {}

    int macroCells () const { return macroCells_; }

    int size ( int codim ) const { return macroCells_ + codim; }

    template< int codim >
    typename Codim< codim >::Geometry geometry ( int i ) const
    {
      int cells = macroCells(); 
      return typename Codim< codim >::Geometry( i, cells, interval_ );
    }

    template< int codim >
    typename Codim< codim >::LocalGeometry localGeometry ( int i ) const
    {
      assert( i <= codim );
      int cells = 1; 
      return typename Codim< codim >::LocalGeometry( i, cells, Interval( 0, 1 ) );
    }

    const Interval &interval () const { return interval_; }

  private:
    const Interval interval_;
    const int macroCells_;
  };



  // PrismInterval
  // -------------

  template< class ct >
  class PrismInterval
  {
    // this type
    typedef PrismInterval< ct > This;

  public:
    typedef ct ctype;

    PrismInterval ( ctype bound[ 2 ] )
    {
      bound_[ 0 ] = std::min( bound[ 0 ], bound[ 1 ] );
      bound_[ 1 ] = std::max( bound[ 0 ], bound[ 1 ] );
      length_ = bound_[ 1 ] - bound_[ 0 ];
    }

    PrismInterval ( ctype a, ctype b )
    {
      bound_[ 0 ] = std::min( a, b );
      bound_[ 1 ] = std::max( a, b ); 
      length_ = bound_[ 1 ] - bound_[ 0 ];
    }

    ctype corner ( int i ) const
    {
      assert ( i == 0 || i == 1 );
      return bound_[ i ];
    }

    ctype length () const { return length_; }

  private:
    ctype bound_[ 2 ];
    ctype length_;
  };



  // PrismNormalGeometry, mydimension = 1
  // ------------------------------------

  template< class ct, int mydim >
  struct PrismNormalGeometry
  {
    static const int mydimension = mydim; 
    dune_static_assert( mydimension == 1, "Wrong template parameter" );

    static const int coorddimension = 1; 

    typedef ct ctype;

    typedef PrismInterval< ctype > Interval;

    typedef ctype LocalCoordinate;
    typedef ctype GlobalCoordinate;

    PrismNormalGeometry ( int i, int cells, const Interval &interval )
    : volume_( interval.length()/cells ),
      origin_( interval.corner( 0 ) + i*volume_ )
    {
      assert( cells > 0 );
      assert( i >= 0 && i < cells );
    }

    int corners () const { return 2; }

    GlobalCoordinate corner ( int i ) const
    {
      assert( i == 0 || i == 1 );
      return (origin_ + i*volume_);
    }

    GlobalCoordinate global ( const LocalCoordinate &local ) const
    {
      return corner( 0 ) + local*volume();
    }

    LocalCoordinate local ( const GlobalCoordinate &global ) const
    {
      LocalCoordinate ret = global - corner( 0 );
      return ( ret /= volume() );
    }

    ctype volume () const { return volume_; }

    GlobalCoordinate center () const { return global( 0.5 ); }

  private:
    ctype volume_;
    ctype origin_;
  };



  // PrismNormalGeometry, mydimension = 0
  // ------------------------------------

  template< class ct >
  struct PrismNormalGeometry< ct, 0 >
  {
    static const int mydimension = 0;
    static const int coorddimension = 1; 

    typedef ct ctype;

    typedef PrismInterval< ctype > Interval;

    typedef ctype GlobalCoordinate;
    typedef ctype LocalCoordinate;

    PrismNormalGeometry ( int i, int cells, const Interval &interval )
    : origin_( interval.corner( 0 ) + i*interval.length()/cells )
    {
      assert( cells > 0 );
      assert( i >= 0 && i <= cells );
    }

    int corners () const { return 1; }

    GlobalCoordinate corner ( int i ) const
    {
      assert( i == 0 );
      return origin_;
    }

    GlobalCoordinate global ( const LocalCoordinate & ) const { return origin_; }

    LocalCoordinate local ( const GlobalCoordinate & ) const { return LocalCoordinate( 1 ); }

    ctype volume () const { return ctype( 1 ); }

    GlobalCoordinate center () const { return origin_; }

  private:
    ctype origin_;
  };

} // end namespace Dune

#endif // #ifndef DUNE_FEM_GRID_PRISMGRID_NORMALGRID_HH
